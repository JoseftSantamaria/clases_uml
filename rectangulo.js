
class Cuerpo_humano{

       //atributos 

    nombre_del_sistema;


    constructor(nombre)
    {
        this.nombre_del_sistema=nombre;
    }

    //metodos

    
    funcion()
    {
        console.log("mi funcion se da a traves de todos los sistemas");
    }

    PartesPrincipales(){

        console.log("cabeza, tronco y extremidades");
    }

}


class Sist_inmunitario extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es proteger a nuestro cuerpo de cualquier infección");
    }


    PartesPrincipales(){

        console.log("medula osea, timo, ganglios linfaticos ,baso y amigdalas")
    }
}


class Sist_Tegumentario extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es separar nuestros órganos internos del medio externo, proceso de excreción");
    }

    PartesPrincipales(){
        
        console.log("Piel, cabello y uñas")
    }
}


class Sist_Linfatico extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es formar y movilizar la linfa (liquido con globulos blancos)");
    }


    PartesPrincipales(){

        console.log("Linfa, Vasos linfáticos y Ganglios linfáticos")
    }

}



class Sist_Muscular extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es permitir el movimiento del cuerpo y darle forma al mismo");
    }

    PartesPrincipales(){

        console.log("Compuesto por mas de 650 musculos")
    }
}



class Sist_Circulatorio extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es movilizar la sangre a todas las partes del cuerpo");
    }

    PartesPrincipales(){

        console.log("Arterias, Capilares, venas y Corazón")
    }
}


class Sist_Nervioso extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es comprender y procesar, de forma muy rápida, las señales externas");
    }

    PartesPrincipales(){

        console.log("encéfalo, médula espinal, nervios craneales y nervios espinales")
    }
}


class Sist_Oseo extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es el apoyo a la estructura de nuestro cuerpo y protegerlo a través de los huesos, además interviene en la formación de células sanguíneas y en la reserva de calcio");
    }
    
    PartesPrincipales(){

        console.log("Compuesto por aproximadamente 206 huesos")
    }
}


class Sist_Articular extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es el movimiento de los huesos, sostener o mantener la unión entre ellas");
    }

    PartesPrincipales(){

        console.log("aprox 200 articulaciones y ligamentos")
    }
}

class Sist_Endocrino extends Cuerpo_humano
{

    funcion()
    {

      console.log("mi funcion es la comunicación por medio de las hormonas que el cuerpo segrega, regular el metabolismo, la función sexual y el crecimiento.");
    }

    PartesPrincipales(){

        console.log("glándulas endocrinas, hipófisis, tiroides, timo, páncreas y glándulas suprarrenales")
    }

}

let inmunitario=new Sist_inmunitario ("Sistema Inmunitario")
let Tegumentario=new Sist_Tegumentario ("Tegumentario")
let Linfatico = new Sist_Linfatico ("Linfatico")
let Muscular = new Sist_Muscular ("Muscular")
let Circulatorio = new Sist_Circulatorio ("Circulatorio")
let Nervioso = new Sist_Nervioso ("Nervioso")
let Oseo = new Sist_Oseo ("Oseo")
let Articular = new Sist_Articular ("Articular")
let endocrino = new Sist_Endocrino ("endocrino")